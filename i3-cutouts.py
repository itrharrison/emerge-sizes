import numpy as np
import pandas as pd
from astropy.io import fits
from astropy.table import Table
from matplotlib import pyplot as plt
from matplotlib import rc
import glob
import pdb
import ConfigParser
import sys

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all')

config = ConfigParser.ConfigParser()
config.read(sys.argv[-1])
im3dir = config.get('im3shape', 'install_directory')

sys.path.append(im3dir)

from py3shape.analyze import analyze, count_varied_params
from py3shape.options import Options

options = Options(config.get('im3shape', 'ini_file'))

psf_stamp = fits.getdata('./cutouts/emerge-psf-circular-390.fits')

idx=0

output_catalogue = Table(names=('x0',
                                'y0',
                                'e1',
                                'e2',
                                'radius',
                                'radius_ratio',
                                'bulge_A',
                                'disc_A',
                                'delta_theta_bulge',
                                'bulge_flux',
                                'disc_flux',
                                'bulge_index',
                                'disc_index',
                                'snr',
                                'likelihood'))

for filename in glob.glob('./cutouts/*.fits'):

  if idx < 190:
    idx += 1
    continue

  stamp = fits.getdata(filename)

  plt.hist(stamp.flatten(), bins=np.linspace(-10, 10, 20), color='k', histtype='step', alpha=0.7)

  weight = np.ones_like(stamp) # ToDo: Should be from RMS map
  # Measure the shear with im3shape
  result, best_fit = analyze(stamp, psf_stamp, options, weight=weight, ID=idx)
  result = result.as_dict(0, count_varied_params(options))

  output_catalogue.add_row([result[0]['x0'],
                            result[0]['y0'],
                            result[0]['e1'],
                            result[0]['e2'],
                            result[0]['radius'],
                            result[0]['radius_ratio'],
                            result[0]['bulge_A'],
                            result[0]['disc_A'],
                            result[0]['delta_theta_bulge'],
                            result[0]['bulge_flux'],
                            result[0]['disc_flux'],
                            result[0]['bulge_index'],
                            result[0]['disc_index'],
                            result[0]['snr'],
                            result[0]['likelihood']])

  idx += 1
  print(idx, np.std(stamp.flatten()))

output_catalogue.write('./output/i3-output-wheader.txt', format='ascii')
