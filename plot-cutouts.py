import numpy as np
import pandas as pd
from astropy.io import fits
from matplotlib import pyplot as plt
from matplotlib import rc
import glob
import pdb

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

for filename in glob.glob('./cutouts/*.fits'):

  im = fits.getdata(filename)
  hdr = fits.getheader(filename)

  plt.close('all')
  plt.figure(1, figsize=(5, 3.75))
  plt.imshow(im, cmap='hot')
  plt.colorbar()
  plt.axis('off')
  plt.savefig(filename+'.png', bbox_inches='tight', dpi=300)
  print(filename, hdr['BMAJ'], hdr['BMIN'], hdr['BPA'])