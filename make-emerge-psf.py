import galsim

npix = 128
upsampling = 3
padding = 2

pix_scale = 1.736111153e-05*galsim.degrees
psf_fwhm = 0.2

image_npix = (npix+padding)*upsampling

psf = galsim.Gaussian(fwhm=psf_fwhm)
psf_stamp = psf.drawImage(nx=image_npix, ny=image_npix, scale=pix_scale/galsim.arcsec)

psf_stamp.write('emerge-psf-circular-{0}.fits'.format(image_npix))