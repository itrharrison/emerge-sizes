import numpy as np
import pandas as pd
from astropy.io import fits
from astropy.table import Table
from matplotlib import pyplot as plt
from matplotlib import rc
import glob
import pdb
try:
  import ConfigParser
except ImportError:
  import configparser as ConfigParser
import sys

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all')

output_catalogue = Table.read('./output/i3-output-wheader.txt.bup', format='ascii')
prob_cat = Table.read('data/outputfile_classed_probabilities.csv', format='csv')

output_catalogue['sfg_probability'] = 1. - prob_cat['27_Muxlow_class'][:len(output_catalogue)]

physical_cut = (output_catalogue['snr']>0)*(output_catalogue['radius']>0)
good_cut = output_catalogue['radius']>2
sfg_cut = output_catalogue['sfg_probability']>0.9

physical_cat = output_catalogue[physical_cut]
good_cat = output_catalogue[physical_cut*good_cut*sfg_cut]
bad_cat = output_catalogue[physical_cut*(~good_cut)]

plt.figure(1, figsize=(5,3.75))
plt.loglog(bad_cat['bulge_A'], bad_cat['disc_A'], 'o', c='lightcoral', label='$0 < r \leq 2$')
plt.loglog(good_cat['bulge_A'], good_cat['disc_A'], 'o', c='powderblue', label='$r > 2 \, \, \mathrm{[pix]}$')
plt.xlim([1e-5, 1e4])
plt.ylim([1e-5, 1e4])
x = [1e-5, 1e4]
plt.plot(x, x, 'k--')
plt.xlabel('$\mathrm{Bulge} \, \, A_{\\rm bulge}$')
plt.ylabel('$\mathrm{Disc} \, \, A_{\\rm disc}$')
plt.legend(frameon=False)
plt.savefig('./plots/bulge_disc.png', dpi=300, bbox_inches='tight')

plt.figure(2, figsize=(5,3.75))
plt.loglog(bad_cat['bulge_flux'], bad_cat['disc_flux'], 'o', c='lightcoral', label='$0 < r \leq 2$')
plt.loglog(good_cat['bulge_flux'], good_cat['disc_flux'], 'o', c='powderblue', label='$r > 2 \, \, \mathrm{[pix]}$')
plt.xlim([1e-5, 1e2])
plt.ylim([1e-5, 1e2])
x = [1e-5, 1e2]
plt.plot(x, x, 'k--')
plt.xlabel('$\mathrm{Bulge \, Flux} \, \, \Phi_{\\rm bulge}$')
plt.ylabel('$\mathrm{Disc \, Flux} \, \, \Phi_{\\rm disc}$')
plt.legend(frameon=False)
plt.savefig('./plots/bulge_flux-disc_flux.png', dpi=300, bbox_inches='tight')

plt.figure(3, figsize=(5,3.75))
plt.subplot(111)
mixed_cut = (physical_cat['bulge_A'] > 0)*(physical_cat['disc_A'] > 0)
mixed_cat = physical_cat[mixed_cut]

disc_cat = physical_cat[(physical_cat['bulge_A'] == 0)]
bulge_cat = physical_cat[(physical_cat['disc_A'] == 0)]

bulge_cat = 1000.*np.ones_like(bulge_cat['bulge_A'])
disc_cat = -100.*np.ones_like(disc_cat['disc_A'])

plt.hist((mixed_cat['bulge_A']/mixed_cat['disc_A']), bins=np.linspace(0,1000,30), histtype='bar', color='lightcoral')
plt.hist(bulge_cat, bins=np.linspace(-20,1050,30), histtype='bar', color='lightcoral')
plt.hist(disc_cat, bins=np.linspace(-100,1000,30), histtype='bar', color='lightcoral', label='$r > 0 \, \, \mathrm{[pix]}$')
plt.xlabel('$\mathrm{Bulge \, to \, Disc \, \, Ratio} \, \, A_{\\rm bulge} / A_{\\rm disc}$')
plt.xlim([-150,1100])
plt.ylim([0,55])
plt.xticks((-85, 0, 200, 400, 600, 800, 1000),('$\mathrm{Disc}$ \n $\mathrm{only}$', '0', '200', '400', '600', '800', '$\mathrm{Bulge}$ \n $\mathrm{only}$'))
#plt.text(800, 50, '$r > 0 \, [\mathrm{pix}] $')

plt.subplot(111)
mixed_cut = (good_cat['bulge_A'] > 0)*(good_cat['disc_A'] > 0)
mixed_cat = good_cat[mixed_cut]

disc_cat = good_cat[(good_cat['bulge_A'] == 0)]
bulge_cat = good_cat[(good_cat['disc_A'] == 0)]

bulge_cat = 1000.*np.ones_like(bulge_cat['bulge_A'])
disc_cat = -100.*np.ones_like(disc_cat['disc_A'])

plt.hist((mixed_cat['bulge_A']/mixed_cat['disc_A']), bins=np.linspace(0,1000,30), histtype='bar', color='powderblue')
plt.hist(bulge_cat, bins=np.linspace(-20,1050,30), histtype='bar', color='powderblue')
plt.hist(disc_cat, bins=np.linspace(-100,1000,30), histtype='bar', color='powderblue', label='$r > 2 \, \, \mathrm{[pix]}$')
plt.xlabel('$\mathrm{Bulge \, to \, Disc \, \, Ratio} \, \, A_{\\rm bulge} / A_{\\rm disc}$')
plt.xlim([-150,1100])
plt.ylim([0,52])
plt.xticks((-85, 0, 200, 400, 600, 800, 1000),('$\mathrm{Disc}$ \n $\mathrm{only}$', '0', '200', '400', '600', '800', '$\mathrm{Bulge}$ \n $\mathrm{only}$'))
#plt.text(800, 50, '$r > 2 \, [\mathrm{pix}] $')
plt.legend(frameon=False)
plt.savefig('./plots/bulge_disc_ratio.png', dpi=300, bbox_inches='tight')

counts_mixed, bins_mixed, _ = plt.hist((mixed_cat['bulge_A']/mixed_cat['disc_A']), bins=np.linspace(0,1000,30))
bins_mixed_centres = (bins_mixed[:-1]+ bins_mixed[1:])/2
n_bulge_only =  bulge_cat.shape[0]
n_disc_only =  disc_cat.shape[0]

ratio_hist_output = Table()
ratio_hist_output['bin_centres'] = bins_mixed_centres
ratio_hist_output['counts'] = counts_mixed
ratio_hist_output.add_row([-np.infty, n_disc_only])
ratio_hist_output.add_row([np.infty, n_bulge_only])
ratio_hist_output.write('./output/ratio_hist_output.fits', format='fits', overwrite=True)
ratio_hist_output.write('./output/ratio_hist_output.txt', format='ascii', overwrite=True)

ratio_output = Table()
ratio_output['bulge_A'] = good_cat['bulge_A']
ratio_output['disc_A'] = good_cat['disc_A']
ratio_output['bulge_flux'] = good_cat['bulge_flux']
ratio_output['disc_flux'] = good_cat['disc_flux']
ratio_output['sfg_probability'] = good_cat['sfg_probability']
ratio_hist_output.write('./output/ratio_output.fits', format='fits', overwrite=True)
ratio_hist_output.write('./output/ratio_output.txt', format='ascii', overwrite=True)

plt.figure(4, figsize=(5,3.75))
plt.subplot(111)
mixed_cut = (physical_cat['bulge_A'] > 0)*(physical_cat['disc_A'] > 0)
mixed_cat = physical_cat[mixed_cut]

disc_cat = physical_cat[(physical_cat['bulge_A'] == 0)]
bulge_cat = physical_cat[(physical_cat['disc_A'] == 0)]

bulge_cat = 1000.*np.ones_like(bulge_cat['bulge_A'])
disc_cat = -100.*np.ones_like(disc_cat['disc_A'])

plt.hist((mixed_cat['bulge_A']/mixed_cat['disc_A']), bins=np.linspace(0,1000,30), histtype='bar', color='powderblue')
plt.hist(bulge_cat, bins=np.linspace(-20,1050,30), histtype='bar', color='lightcoral')
plt.hist(disc_cat, bins=np.linspace(-100,1000,30), histtype='bar', color='lightcoral', label='$r > 0 \, \, \mathrm{[pix]}$')
plt.xlabel('$\mathrm{Bulge \, to \, Disc \, \, Ratio} \, \, A_{\\rm bulge} / A_{\\rm disc}$')
plt.xlim([-150,1100])
plt.ylim([0,52])
plt.xticks((-85, 0, 200, 400, 600, 800, 1000),('$\mathrm{Disc}$ \n $\mathrm{only}$', '0', '200', '400', '600', '800', '$\mathrm{Bulge}$ \n $\mathrm{only}$'))
plt.savefig('./plots/bulge_disc_ratio-all.png', dpi=300, bbox_inches='tight')

